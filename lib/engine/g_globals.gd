# -----------------------------------------------------------------
# Class:          		Globals
# Author:         		Navred
# Notes:          		Singleton
# Date:					11/2020
# Description:			Globals singleton
# -----------------------------------------------------------------


extends Node2D

# Managers
var managers_scn = preload("res://scenes/managers/managers.tscn")

# The manager service locator
var managers

# The global music engine
var music_engine

# The global sound engine
var sound_engine

# The global modulate
var screen_modulate

# The global animation
var animation

# The bullet pool
var bullet

#
# Globals initialization
#
func _ready():
	add_child(managers_scn.instance())

	managers = get_node("managers")
	music_engine = managers.get_node("music")
	sound_engine = managers.get_node("sound")
	screen_modulate = managers.get_node("modulate")
	animation = managers.get_node("animation")
