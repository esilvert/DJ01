#
# g_player_manager
#

extends Node2D

signal player_respawn
signal collected # (name)

var player = null
var spawners = []

var collectible_count = 0

func _ready():
	connect('collected', self, '_on_collected')

func respawn():
	spawners.back().spawn(player)
	
func _on_collected(c):
	collectible_count += 1
	
	if collectible_count == 3: 
		g_scene_manager.goto_scene('end')