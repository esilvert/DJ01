class_name FSMAnimator

extends AnimationPlayer

onready var fsm = get_node('../fsm')

func _ready():
	# FSM
	fsm.connect('stateChanged', self, "_on_state_changed")

# Callback - Play anim on state changed
func _on_state_changed(p_current, p_new):
	prints('FSMAnimator:', 'Playing anim for state', p_current)
	play(p_current)
