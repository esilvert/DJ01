*Theme: Une apparition inattendue*

* Apparition
- [ ] Fantôme
- [ ] Reflet dans le miroir
- [ ] Indice
- [ ] Maxi easter egg (nul)
- [ ] Dieu
- [ ] Satan
- [ ] Menace inconnue

* Inatendue
- [ ] A la mort
- [ ] A la fin du jeu
- [ ] Quand tu n'as plus d'espoir
- [ ] Juste avant de gagner
- [ ] N'importe quand qui marque la fin
- [ ] Dans coin d'un labyrinthe
- [ ] Dans l'ombre

* Bilan
- [ ] Die & retry
- [ ] Un fantome à la mort
- [ ] Satan juste avant de gagner
- [ ] Indice à la mort (par dieu ?)
- [ ] Se cacher dans l'ombre ou parfois se terre une menace
  - Récupérer un objet release une malediction
  - Fuir les danger immédiats et constater les dangers innatendu sous plusieurs formes

* DA
** Music
- [ ] Music spooky, parfois scalme, parfois rythmée
** Graphisme
- [ ] Masques sombres | B&W with true color
- [ ] Smooth !!! Prioriser le smooth au beau
** Gameplay
- [ ] Calme mais pas inerte
- [ ] Moments d'accélération
- [ ] Pourchasser un objectif / fuir un danger grandissant

|-----------------------------------+---------+-----------------------+--------+------------+------+-------+-----------------|
| v Inatendu \ > Apparition         | Fantôme | Reflet dans le miroir | Indice | Easter egg | Dieu | Satan | Menace inconnue |
|-----------------------------------+---------+-----------------------+--------+------------+------+-------+-----------------|
| A la mort                         | Y       |                       | 0      | y          | y    |       |                 |
| A la fin du jeu                   | Y       |                       |        |            | -    |       |                 |
| Quand tu n'as plus d'espoir       | y       | y                     | y      |            | -    |       |                 |
| Juste avant de gagner             |         |                       |        |            |      | y     |                 |
| N'importe quand qui marque la fin |         |                       |        |            |      |       | Y               |
| Dans un coin du labirynthe        | -       |                       |        |            |      |       | y               |
| Dans l'ombre                      |         |                       |        |            |      |       |                 |
|                                   |         |                       |        |            |      |       |                 |
